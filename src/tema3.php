<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Delgado IS4 2do Parcial Tema 3</title>
  <style>
    table {border-collapse: collapse;}
    td {border: 1px solid black; padding: 4px}
  </style>
</head>
<body>
  <table>
    <thead>
      <tr>
        <th>Índice</th>
        <th>Valor</th>
      </tr>
    </thead>
    <tbody>
      
<?php 
  function randomCode() {
    $letters = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'];
    $code = '';
    for ($i = 0; $i < 4; $i++) {
      $code = $code.$letters[mt_rand(0, 25)];
    } 

    return $code; 
  }
  $items = [];
  for ($i = 0; $i < 500; $i++) {
    array_push($items, randomCode());
  }

  foreach ($items as $key => $value) {
?>
  <tr>
    <td><?php echo $key;?></td>
    <td><?php echo $value;?></td>
  </tr>
<?php 
  }
?>
    </tbody>
  </table>
</body>
</html>