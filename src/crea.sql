CREATE DATABASE examen;
CREATE TABLE IF NOT EXISTS alumnos (
  id INT,
  nombre STR,
  apellido STR,
  edad INT
);