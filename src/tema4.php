<?php 
  $alumnos = [
    [1, 'Monica', 'Geller', 23],
    [2, 'Chandler', 'Bing', 24], 
    [3, 'Ross', 'Geller', 25],
    [4, 'Rachel', 'Green', 26],
    [5, 'Joey', 'Tribiani', 27],
    [6, 'Ted', 'Mosby', 28],
    [7, 'Lily', 'Aldrin', 29], 
    [8, 'Robin', 'Scherbatsky', 30],
    [9, 'Marshall', 'Erikssen', 31],
    [10, 'Barney', 'Stinson', 32],
    [11, 'Britta', 'Perry', 33],
    [12, 'Annie', 'Edison', 34]
  ]
  try{
    $conn = new PDO('pgsql:host=localhost;dbname=examen;','postgres', 'postgres');
    $insert = 'insert into alumnos (id, nombre, apelllido, edad) values (?,?,?,?)';

    foreach ($alumnos as $alumno) {
      $statement = $conn->prepare($insert);
      foreach ($alumno as $item) {
        $statement->bindParam(1, $item[0], PDO::PARAM_INT);
        $statement->bindParam(2, $item[1], PDO::PARAM_STR);
        $statement->bindParam(3, $item[2], PDO::PARAM_STR);
        $statement->bindParam(4, $item[3], PDO::PARAM_INT);
      }
      $count = $statement->execute();
    }

    $queryMax = 'select nombre, apellido, max(edad) from alumnos';
    $queryMin = 'select nombre, apellido, min(edad) from alumnos';

    $max = $conn->prepare($queryMax);
    $max->execute;
    $maxes = $max->fetchAll();

    $min = $conn->prepare($queryMin);
    $min->execute;
    $mins = $min->fetchAll();

    $conn = null;
    
  } catch(PODException $e) {
    echo "Error: ".$e->getMessage();
  }
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Delgado IS4 2do Parcial Tema 4</title>
</head>
<body>
  <table>
    <thead>
      <tr>
        <th colspan=3>Máxima edad</th>
      </tr>
      <tr>
        <th>Nombre</th>
        <th>Apellido</th>
        <th>Edad</th>
      </tr>
    </thead>
    <tbody>
  <?php 
    foreach ($maxes as $row) {
      ?>
      <tr>
        <td><?php echo $row['nombre'] ?></td>
        <td><?php echo $row['apellido'] ?></td>
        <td><?php echo $row['edad'] ?></td>
      </tr>
      <?php
    }
  ?> 
    </tbody>
  </table>
  <br>
  <table>
    <thead>
      <tr>
        <th colspan=3>Mínima edad</th>
      </tr>
      <tr>
        <th>Nombre</th>
        <th>Apellido</th>
        <th>Edad</th>
      </tr>
    </thead>
    <tbody>
  <?php 
    foreach ($mins as $row) {
      ?>
      <tr>
        <td><?php echo $row['nombre'] ?></td>
        <td><?php echo $row['apellido'] ?></td>
        <td><?php echo $row['edad'] ?></td>
      </tr>
      <?php
    }
  ?> 
    </tbody>
  </table>
</body>
</html>